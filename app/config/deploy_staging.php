<?php

use EasyCorp\Bundle\EasyDeployBundle\Deployer\DefaultDeployer;

return new class extends DefaultDeployer
{
    public function configure()
    {
        return $this->getConfigBuilder()
            // SSH connection string to connect to the remote server (format: user@host-or-IP:port-number)
            ->server('root@staging-api.poolparty.games')
            ->useSshAgentForwarding(false)
            ->remoteComposerBinaryPath('composer')
            ->composerInstallFlags('--no-interaction --prefer-dist --no-dev')

            ->sharedFilesAndDirs(['web/uploads', 'var/logs', 'var/jwt'])
            // the absolute path of the remote server directory where the project is deployed
            ->deployDir('/var/www/html/poolparty-api')
            // the URL of the Git repository where the project code is hosted
            ->repositoryUrl('git@bitbucket.org:poolpartyoxmose/oxmose-api.git')

            // the repository branch to deploy
            ->repositoryBranch('develop')

            ->keepReleases(2)
        ;
    }

    // run some local or remote commands before the deployment is started
    public function beforeStartingDeploy()
    {
        // $this->runLocal('./vendor/bin/simple-phpunit');
    }

    public function beforePreparing()
    {
        $this->runRemote('cp {{ project_dir }}/app/config/parameters_staging.yml {{ project_dir }}/app/config/parameters.yml');
    }


    // run some local or remote commands after the deployment is finished
    public function beforeFinishingDeploy()
    {
        $this->runRemote('{{ console_bin }} do:sc:update --force');

        $url = 'https://discordapp.com/api/webhooks/403130433564573696/wFeD-OIXT8m_ixJ4AsB9lRTzB_PjHDihl5BrN2pXSeGOprQrgqKErMoOSN_AcdAI3tRZ';
        $data = json_encode(['name' => 'Toto', 'content' => 'Déployé sur staging', 'avatar' => null]);
        $this->runLocal("curl -XPOST -H 'Content-type: application/json' --data '${data}'  ${url}");
        // $this->runRemote('{{ console_bin }} app:my-task-name');
        // $this->runLocal('say "The deployment has finished."');
    }
};
