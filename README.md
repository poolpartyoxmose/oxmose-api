# API 

Ce projet basé sur Symfony 3.3 fonctionne sous PHP 7 et +.

Afin de faire fonctionner ce projet en standalone, une configuration docker a été crée. 

Vous pouvez utiliser ce projet sans docker, cependant vous devrez configurer vous même votre serveur.

## Configuration avec docker

Il vous faut docker d'installé sur votre machine. Une fois installé et parametré.

Dans une console dans le dossier racine du projet, faire :
``
docker-compose build
``
Cela créera les différents container
Pour lancer le container
``
docker-compose up -d
``
Docker tournera en arrière plan.
Afin de voir les différentes images en cours.
``
docker ps
``

Pour rentrer dans symfony et executer des commandes en consoles: 
``
docker-compose exec php bash
``

## Installation

Installer les packages pour la prod, pour du dev, ne pas ajouter le flag `--no-dev`

``
composer install --no-dev
``

Fetcher les vendors pour adminLTE

```
php bin/console assets:install --symlink

php bin/console avanzu:admin:fetch-vendor
```


Il faut aussi créer les clés pour le jwt: 
```
$ mkdir -p var/jwt # For Symfony3+, no need of the -p option
$ openssl genrsa -out var/jwt/private.pem -aes256 4096
$ openssl rsa -pubout -in var/jwt/private.pem -out var/jwt/public.pem
```

## Dépendance externe

Certaines librairies ont été ajoutés afin d'avoir plusieurs fonctionnalités pré-établis.

### KitpagesDatagridBundle

Ce bundle permet de faire du listing d'entités.

[Documentation](https://github.com/kitpages/KitpagesDataGridBundle/blob/master/Resources/doc/10-GridExtendedUse.md)

Outre le fait qu'il fait du listing. J'ai mis en place une surcouche afin d'éviter de tout faire dans le controller.
Il y a un Transformer: src/AppBundle/Transformer/ListToGridTransformer.php

Il récupère la configuration présente dans app/config/admin/list.yml afin de faire la correspondance des champs et y ajouter facilement des options.

Il suffit dans le controleur de créer son queryBuilder avec un alias et ensuite de passer tout cela dans le transformer afin d'avoir une instance propre du GridConfig.

On peut toujours y ajouter ou retirer des choses dans le controleur si besoin.
