<?php
namespace AppBundle\Controller\Admin;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 * @Route(path="/admin")
 * @package AppBundle\Controller\Admin
 */
class DefaultController extends Controller {


    /**
     * @Route(path="/", name="admin_homepage")
     */
    public function index() {
        return $this->render('AppBundle:Admin:base.html.twig');
    }
}