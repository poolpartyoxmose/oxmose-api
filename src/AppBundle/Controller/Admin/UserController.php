<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Transformer\ListToGridTransformer;
use Kitpages\DataGridBundle\Grid\Field;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(path="/admin/users")
 * Class UserController
 * @package AppBundle\Controller\Admin
 */
class UserController extends Controller
{

    /**
     * @Route(path="/", name="admin_users_list")
     */
    public function listUsers(Request $request, ListToGridTransformer $gridTransformer)
    {

        $users = $this->getDoctrine()->getRepository('AppBundle:User');

        $queryBuilder = $users->createQueryBuilder('u');

        $gridConfig = $gridTransformer->getGrid('users', 'u', $queryBuilder);

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $request);

        return $this->render('AppBundle:Admin/CRUD:list.html.twig', [
            'grid' => $grid
        ]);
    }
}
