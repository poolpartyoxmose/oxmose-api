<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\Security\ResettingType;
use AppBundle\Service\SecurityService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends Controller
{
    /**
     * @Route(name="login", path="/login")
     */
    public function loginAction(Request $request, AuthenticationUtils $authenticationUtils)
    {
        $error = $authenticationUtils->getLastAuthenticationError();

        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('AppBundle:Security:login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }

    /**
     * @Route(name="request_password", path="/request_password")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function requestPassword(SecurityService $service, Request $request)
    {
        $email = $request->request->get('email');
        if ($request->isMethod('POST')) {
            if ($email !== null && $email !== '') {
                $user = $this->getDoctrine()->getManager()->getRepository('AppBundle:User')->findAll()[0];
                $service->generateToken($user);
                return $this->redirect($this->generateUrl('request_password_confirm'));
            } else {
                $this->addFlash('error', 'Merci d\'indiquer une adresse email');
            }

        }
        return $this->render('AppBundle:Security:request_password.html.twig');
    }

    /**
     * @Route(name="request_password_confirm", path="/request_password/confirmed")
     */
    public function confirmRequestPassword()
    {
        return $this->render('AppBundle:Security:request_password_confirmed.html.twig');
    }

    /**
     * @Route(name="renew_password", path="/request_password/renew/{token}")
     * @param SecurityService $service
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function renewPassword($token, SecurityService $service, Request $request)
    {
        /** @var User $user */
        $user = $service->getUserByToken($token);
        if ($user !== null && $user->isTokenValid()) {
            $form = $this->createForm(ResettingType::class, $user);
            if ($request->isMethod('POST')) {

                $form->handleRequest($request);
                if ($form->isValid()) {
                    $service->savePlainPassword($user);
                    $this->addFlash('success', 'Mot de passe changé avec succès');
                    return $this->redirect($this->generateUrl('login'));
                }
            }

            return $this->render('AppBundle:Security:renew_password.html.twig', [
                'form' => $form->createView()
            ]);

        } else {
            $this->addFlash('error', 'Impossible de retrouver l\'utilisateur lié a ce token, merci de vérifier celui-ci');
            return $this->redirect($this->generateUrl('login'));
        }
    }

    /**
     * @Route(name="facebook_register", path="/register/facebook")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function registerFacebook() {
        return $this->render('@AppBundle/Security/register.html.twig');
    }
}
