<?php

namespace AppBundle\Service;


use AppBundle\Entity\User;
use AppBundle\Mailer\MailerFactory;
use AppBundle\Mailer\MailerService;
use AppBundle\Mailer\Security\SecurityTokenPasswordMail;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class SecurityService
{


    /**
     * @var TokenGeneratorInterface
     */
    private $tokenGenerator;
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var MailerService
     */
    private $mailerService;


    public function __construct(TokenGeneratorInterface $tokenGenerator, EntityManager $entityManager, UserPasswordEncoderInterface $passwordEncoder, LoggerInterface $logger, MailerService $mailerService)
    {
        $this->tokenGenerator = $tokenGenerator;
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->logger = $logger;
        $this->mailerService = $mailerService;
    }

    /**
     * The given user will receive an email with a token
     * The token allow the user to renew his password
     *
     * @param User $user
     */
    public function generateToken(User $user)
    {
        $token = $this->tokenGenerator->generateToken();
        $user->setConfirmationToken($token);
        $user->setConfirmationDate(new \DateTime('+1 day'));

        $this->mailerService->generateEmail('security_request_password', $user->getEmail(), [
            'data' => ['token' => $token, 'user' => $user]])->send();
        $this->entityManager->flush($user);
    }

    /**
     * @param $token
     * @return User
     */
    public function getUserByToken($token)
    {
        /** @var User|null $user */
        return $this->entityManager->getRepository('AppBundle:User')->findOneByConfirmationToken($token);
    }

    public function setPlainPassword(User $user)
    {
        if ($user->getPlainPassword() !== null) {
            $password = $this->passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

        } else {
            throw new \Exception('No plain password provided');
        }
        return $user;
    }

    public function savePlainPassword(User $user)
    {
        $user = $this->setPlainPassword($user);
        $user->setConfirmationDate(null);
        $user->setConfirmationToken(null);
        $this->entityManager->flush($user);
    }
}
