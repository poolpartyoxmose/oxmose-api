<?php

namespace AppBundle\Service;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserService
{

    /**
     * @var JWTManager
     */
    private $JWTManager;
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var SecurityService
     */
    private $securityService;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(JWTManager $JWTManager, EntityManager $entityManager, SecurityService $securityService, ValidatorInterface $validator)
    {
        $this->JWTManager = $JWTManager;
        $this->entityManager = $entityManager;
        $this->securityService = $securityService;
        $this->validator = $validator;
    }

    /**
     * Generate a token for the provided user
     * @param $user
     * @return string
     */
    public function generateToken($user)
    {
        return $this->JWTManager->create($user);
    }

    /**
     * @param User $user
     */
    public function updateUser(User $user)
    {
        $this->securityService->savePlainPassword($user);
        $this->entityManager->flush($user);
    }

    /**
     * @param User $user
     * @return User
     */
    public function saveUser(User $user) {
        //Basic role
        $user->setRoles(['ROLE_USER']);
        $this->securityService->setPlainPassword($user);
        return $user;
    }
}
