<?php
namespace AppBundle\DataFixtures;

use AppBundle\Entity\Users;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class LoadUser extends  Fixture {

    private $passwordEncoder ;

    /**
     * LoadUser constructor.
     * @param $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }


    public function load(ObjectManager $manager)
    {
        $adminUser = new Users();

        $adminUser->setUsername('admin');
        $adminUser->setFirstName('admin');
        $adminUser->setLastName('admin');
        $adminUser->setEnabled(true);
        $adminUser->setEmail('admin@admin.com');
        $adminUser->setRoles(['ROLE_ADMIN']);
        $passwordEncoded = $this->passwordEncoder->encodePassword($adminUser,'admin');
        $adminUser->setPassword($passwordEncoded);

        $manager->persist($adminUser);
        $manager->flush();
    }


}