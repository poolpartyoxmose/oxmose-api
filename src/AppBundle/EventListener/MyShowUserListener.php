<?php
namespace AppBundle\EventListener;


use Avanzu\AdminThemeBundle\Event\ShowUserEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class MyShowUserListener
{

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function onShowUser(ShowUserEvent $event) {
         $user = $this->getUser();
         $event->setUser($user);

     }

    protected function getUser() {
        return $this->tokenStorage->getToken()->getUser();
    }
}
