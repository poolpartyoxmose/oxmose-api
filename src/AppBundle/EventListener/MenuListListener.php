<?php

namespace AppBundle\EventListener;

use AppBundle\Menu\MenuItemModel;
use Avanzu\AdminThemeBundle\Event\SidebarMenuEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Yaml\Yaml;

class MenuListListener
{

    private $menu;

    public function __construct($menu)
    {
        $this->menu = $menu;
    }

    public function onSetupMenu(SidebarMenuEvent $event)
    {

        $request = $event->getRequest();

        foreach ($this->getMenu($request) as $item) {
            $event->addItem($item);
        }

    }

    private function addItem($key, $data)
    {
        $options = $data['options'] ?? [];
        $title = $data['title'] ?? 'No title';
        $class = $data['class'] ?? '';
        $route = $data['route'] ?? 'admin_homepage';
        return new MenuItemModel($key, $title, $route, $options, $class);
    }

    protected function getMenu(Request $request)
    {
        $aMenu = Yaml::parse(file_get_contents($this->menu));

        $menuItems = [];

        foreach ($aMenu as $key => $menu) {
            if (!isset($menu['childs'])) {
                $menuItems[] = $this->addItem($key, $menu);
            } else {
                $childs = array_map(function ($item, $key) {
                    return $this->addItem($key, $item);
                }, $menu['childs'], array_keys($menu['childs']));
                $parentMenu = $this->addItem($key, $menu);
                foreach ($childs as $child) {
                    $parentMenu->addChild($child);
                }
                $menuItems[] = $parentMenu;
            }
        }


        // Build your menu here by constructing a MenuItemModel array
//        $menuItems = [
//            $blog = new MenuItemModel('Home', 'Accueil', 'admin_homepage', [], 'fa fa-home')
//        ];


//        $menuItems[] = new MenuItemModel('Users', 'Utilisateurs', 'admin_users_list');

        // Add some children

        // A child with an icon
//        $blog->addChild(new MenuItemModel('ChildOneItemId', 'ChildOneDisplayName', 'child_1_route', array(), 'fa fa-rss-square'));

        // A child with default circle icon
//        $blog->addChild(new MenuItemModel('ChildTwoItemId', 'ChildTwoDisplayName', 'child_2_route'));

        return $this->activateByRoute($request->get('_route'), $menuItems);
    }

    protected function activateByRoute($route, $items)
    {

        foreach ($items as $item) {
            if ($item->hasChildren()) {
                $this->activateByRoute($route, $item->getChildren());
            } else {
                if ($item->getRoute() == $route) {
                    $item->setIsActive(true);
                }
            }
        }

        return $items;
    }

}
