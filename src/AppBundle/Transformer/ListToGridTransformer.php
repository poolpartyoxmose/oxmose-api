<?php

namespace AppBundle\Transformer;

use Doctrine\ORM\QueryBuilder;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Symfony\Component\Yaml\Yaml;

class ListToGridTransformer
{

    private $listFile;

    public function __construct($listFile)
    {
        $this->listFile = $listFile;
    }


    public function getFields($key)
    {
        $data = Yaml::parse(file_get_contents($this->listFile));

        if (array_key_exists($key, $data)) {
            return $data[$key];
        } else {
            throw new \Exception('Key doesn\'t exist for :' . $key);
        }
    }

    /**
     * @param string $list Représente l'index qui sera récupéré depuis le fichier YAML
     * @param string $alias Représente l'alias du queryBuilder
     * @param QueryBuilder $querybuilder Représente la requete
     * @return GridConfig
     */
    public function getGrid($list, $alias, $querybuilder)
    {
        $grid = new GridConfig();
        $grid->setQueryBuilder($querybuilder)
            ->setCountFieldName($alias . '.id');
        $fields = $this->getFields($list);

        foreach ($fields as $key => $field) {
            $grid->addField("{$alias}.{$key}", $field['options']);

            //Si un type est définit et que c'est de type date alors on peut formater celle-ci
            if (isset($field['type']) && $field['type'] === 'date') {
                $gridField = $grid->getFieldByName("{$alias}.{$key}");
                $gridField->setFormatValueCallback(function ($value) use ($field) {
                    return $value->format($field['format']);
                });
            }
        }

        return $grid;
    }
}
