<?php
namespace AppBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Kitpages\DataGridBundle\KitpagesDataGridEvents;
use Kitpages\DataGridBundle\Event\DataGridEvent;

class KitpagesGridSubscriber
    implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(
            KitpagesDataGridEvents::ON_DISPLAY_GRID_VALUE_CONVERSION => 'onConversion',
            KitpagesDataGridEvents::AFTER_DISPLAY_GRID_VALUE_CONVERSION => 'afterConversion'
        );
    }

    public function onConversion(DataGridEvent $event)
    {
        // prevent default formatting
        $event->preventDefault();

        // get value to display
        $value = $event->get("value");

        // datetime formatting
        if ($value instanceof \DateTime) {
            $formatter = new \IntlDateFormatter(
                'fr',
                \IntlDateFormatter::LONG,
                \IntlDateFormatter::NONE,
                $value->getTimezone()->getName(),
                \IntlDateFormatter::GREGORIAN
            );
            $event->set("returnValue", $formatter->format($value->getTimestamp()));
            return;
        }

        // email formating (note the way the autoEscape is modified)
        $field = $event->get("field");
        if (strpos($field->getFieldName(), "email") !== false) {
            $field->setAutoEscape(false);
            $ret = '<a href="mailto:'.$value.'">'.$value.'</a>';
            $event->set("returnValue", $ret);
            return;
        }

        try {
            if($field->getData('type') === 'bool') {
                if($value !== null) {
                    $data = 'oui';
                }else {
                    $data = 'non';
                }
                $event->set("returnValue", $data);
                return;
            }
        }catch (\Exception $e) {

        }



        // for the other cases, copy the value to the return value
        $event->set("returnValue", $value);
    }

    public function afterConversion(DataGridEvent $event)
    {
        // set to uppercase all lastnames
        $field = $event->get("field");
        if (strpos(strtolower($field->getFieldName()), "lastname") !== false) {
            $event->set("returnValue", strtoupper($event->get("returnValue")));
            return;
        }
    }
}
