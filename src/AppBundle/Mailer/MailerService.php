<?php

namespace AppBundle\Mailer;


use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Yaml\Yaml;

class MailerService
{

    /**
     * @var \Swift_Mailer
     */
    private $mailer;
    /**
     * @var TwigEngine
     */
    private $twigEngine;

    /**
     * @var mixed
     */
    private $emailList;

    /**
     * @var \Swift_Message
     */
    private $email;

    private $environment;

    /**
     * @var array
     */
    private $templateData = [];

    private $from;

    public function __construct(\Swift_Mailer $mailer, TwigEngine $twigEngine, $emailList, $environment, $from)
    {
        $this->mailer = $mailer;
        $this->twigEngine = $twigEngine;
        $this->emailList = Yaml::parse(file_get_contents($emailList))['email'];
        $this->email = new \Swift_Message();
        $this->environment = $environment;
        $this->from = $from;

        $this->email->setFrom($this->from);
    }


    /**
     * @param null|string $type
     * @param $to
     * @param array $data
     * @return $this
     */
    public function generateEmail(?string $type = '', $to, $data = array())
    {
        $emailData = $this->extractDataByType($type);

        if ($data['data'] !== null) {
            $this->templateData = $data['data'];
        }

        $this->setTo($to);

        $this->handleEmailData($emailData);
        return $this;
    }

    /**
     * Envoi l'email une fois que celui ci a été généré
     */
    public function send()
    {
        $this->mailer->send($this->email);
    }


    /**
     * Ajoute un fichier en pièce jointe
     * @param $filePath
     * @return $this
     * @throws \Exception
     */
    public function addAttachment($filePath)
    {
        if (file_exists($filePath)) {
            $this->email->attach(
                \Swift_Attachment::fromPath($filePath)
            );
        } else {
            throw new \Exception('No file found in ' . $filePath);
        }
        return $this;
    }


    /**
     * Boucle sur les paramètres du fichier yaml et essai de setter les valeurs. Il faut donc être sur que les methodes existent
     * @param $emailData
     */
    protected function handleEmailData($emailData)
    {
        foreach ($emailData as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            } else {
                throw new \BadMethodCallException('Method ' . $method . ' does not exists.');
            }
        }
    }

    protected function setTo($to)
    {
        $this->email->setTo($to);
    }

    protected function setSubject($subject)
    {
        $this->email->setSubject($subject);
    }

    protected function setTemplate($template)
    {
        $this->email->setBody($this->twigEngine->render(
            $template,
            $this->templateData,
            'text/html'
        ));
    }


    /**
     * Retourne le tableau d'information pour l'email selectionné
     * @param $type
     * @return null
     * @throws \Exception
     */
    protected function extractDataByType($type)
    {
        if ($type !== null && $type !== '') {
            foreach ($this->emailList as $eKey => $eValue) {
                if ($type === $eKey) {
                    return $eValue;
                }
            }
        } else {
            throw new \Exception('Merci d\'indiquer un type valable');
        }
        return null;
    }
}
