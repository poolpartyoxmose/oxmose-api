<?php

namespace AppBundle\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Avanzu\AdminThemeBundle\Model\UserInterface as ThemeUser;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Users
 * @ApiResource(
 *     attributes={
 *          "normalization_context" = { "groups" = { "user_read" } }
 *     },
 *     itemOperations={
 *     "get" ={"method"="GET"},
 *     "special"={"route_name"="api_request_password"},
 *     "login_facebook"={
 *          "method"="POST",
 *          "route_name"="api_login_facebook",
 *          "shortName"="Create user by provided facebook token",
 *          "description"="Log or register (if no exist) user by the provided token"}
 *     },
 *     collectionOperations={
 *     "get" ={"method"="GET"},
 *      "user_info" = {
 *         "method" = "GET",
 *         "route_name" = "api_user_info",
 *         "normalization_context"={"groups"={"user_me_read"}}
 *       },
 *     "user_register"={
 *      "method"="POST",
 *      "route_name"="api_user_register",
 *      }
 *    }
 * )
 *
 * @UniqueEntity("email")
 * @UniqueEntity("username")
 * @ORM\Table(name="oxmose_users")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UsersRepository")
 */
class User implements UserInterface, \Serializable, ThemeUser
{
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }


    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate() {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"user_me_read", "user_read"})
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="username", type="string", length=255)
     * @Groups({"user_me_read", "user_read"})
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255, nullable=true)
     * @Groups({"user_me_read", "user_read"})
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255, nullable=true)
     * @Groups({"user_me_read", "user_read"})
     */
    private $lastName;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Email()
     *
     * @ORM\Column(name="email", type="string", length=255)
     * @Groups({"user_me_read", "user_read"})
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastLogin", type="datetime", nullable=true)
     */
    private $lastLogin;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=true)
     * @Groups({"user_me_read", "user_read"})
     */
    private $enabled;

    /**
     * @var array
     *
     * @ORM\Column(type="array", nullable=true)
     * @Groups({"user_me_read", "user_read"})
     */
    private $roles;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"user_me_read", "user_read"})
     */
    private $confirmationToken;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"user_me_read", "user_read"})
     */
    private $confirmation_date;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"user_me_read","user_read"})
     */
    private $facebookId;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"user_me_read","user_read"})
     */
    private $facebookToken;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"user_me_read","user_read"})
     */
    private $googleId;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"user_me_read","user_read"})
     */
    private $googleToken;

    protected $plainPassword;

    //ADMIN LTE INTERFACE
    public function getAvatar()
    {
        return null;
    }

    public function getName()
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    public function getMemberSince()
    {
        return $this->createdAt->format('d/m/Y');
    }

    public function isOnline()
    {
        return true;
    }

    public function getIdentifier()
    {
        return null;
    }

    public function getTitle()
    {
        return null;
    }
    //END ADMIN LTE INTERFACE

    /**
     * @return bool
     */
    public function isTokenValid() {
        return $this->getConfirmationDate() > new \DateTime();
    }



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param string $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set lastLogin
     *
     * @param \DateTime $lastLogin
     *
     * @return User
     */
    public function setLastLogin($lastLogin)
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * Get lastLogin
     *
     * @return \DateTime
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @param array $roles
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
    }



    public function getRoles()
    {
        return $this->roles;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
    }

    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
        ));
    }

    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            ) = unserialize($serialized);
    }

    /**
     * @return string
     */
    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    /**
     * @param string $confirmation_token
     * @return User
     */
    public function setConfirmationToken($confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getConfirmationDate()
    {
        return $this->confirmation_date;
    }

    /**
     * @param \DateTime $confirmation_date
     * @return User
     */
    public function setConfirmationDate($confirmation_date)
    {
        $this->confirmation_date = $confirmation_date;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     * @return User
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }

    /**
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * @param string $facebookId
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;
    }

    /**
     * @return string
     */
    public function getFacebookToken()
    {
        return $this->facebookToken;
    }

    /**
     * @param string $facebookToken
     */
    public function setFacebookToken($facebookToken)
    {
        $this->facebookToken = $facebookToken;
    }

    /**
     * @return string
     */
    public function getGoogleId()
    {
        return $this->googleId;
    }

    /**
     * @param string $googleId
     */
    public function setGoogleId($googleId)
    {
        $this->googleId = $googleId;
    }

    /**
     * @return string
     */
    public function getGoogleToken()
    {
        return $this->googleToken;
    }

    /**
     * @param string $googleToken
     */
    public function setGoogleToken($googleToken)
    {
        $this->googleToken = $googleToken;
    }

}

