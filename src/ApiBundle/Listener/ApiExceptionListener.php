<?php

namespace ApiBundle\Listener;

use ApiBundle\Exception\ApiException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class ApiExceptionListener
{

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if ( ! $event->getException() instanceof ApiException) {
            return;
        }
        $response = new JsonResponse($event->getException()->getJsonMessage(), 400);
        $event->setResponse($response);
    }
}
