<?php

namespace ApiBundle\Action\Security;

use ApiBundle\Authenticator\GoogleAuth;
use ApiBundle\Exception\ApiException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class LoginGoogleAction {


    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var GoogleAuth
     */
    private $googleAuth;

    public function __construct(RequestStack $requestStack, GoogleAuth $googleAuth)
    {
        $this->requestStack = $requestStack;
        $this->googleAuth = $googleAuth;
    }

    /**
     * @Route(name="api_login_google", path="/login/google")
     * LoginGoogleAction constructor.
     * @param RequestStack $requestStack
     */
    public function __invoke()
    {
        $data = json_decode($this->requestStack->getCurrentRequest()->getContent(), true);
        $googleToken = $data['token'];

        if(!$googleToken) throw new BadRequestHttpException('Token missing');

        $token = $this->googleAuth->login($googleToken);

        return new JsonResponse(['token' => $token], 200);
    }
}
