<?php
namespace ApiBundle\Action\Security;

use AppBundle\Service\SecurityService;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class RenewPasswordAction {


    /**
     * @Route(path="/password/request", name="api_request_password")
     * @Method({"POST"})
     */
    public function __invoke(Request $request, SecurityService $securityService, EntityManager $manager)
    {
        $username = json_decode($request->getContent(), true)['username'];
        if($username === null) {
            throw new BadRequestHttpException('Merci d\'indiquer un nom d\'utilisateur');
        }

        $user = $manager->getRepository('AppBundle:User')->findOneByUsername($username);

        if($user !== null) {
            $securityService->generateToken($user);
        }

        return new JsonResponse(['status' => 'success'], 200);
    }
}
