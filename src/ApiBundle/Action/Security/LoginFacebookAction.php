<?php

namespace ApiBundle\Action\Security;

use ApiBundle\Authenticator\FacebookAuth;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;

class LoginFacebookAction
{
    private $facebookAuth;

    private $request;

    public function __construct(FacebookAuth $facebookAuth, RequestStack $request)
    {
        $this->facebookAuth = $facebookAuth;
        $this->request = $request;
    }

    /**
     * @Route(name="api_login_facebook", path="login/facebook")
     * @Method({"POST"})
     */
    public function __invoke()
    {
        $datas = json_decode($this->request->getCurrentRequest()->getContent(), true);
        $token = $this->facebookAuth->login($datas['token']);

        return new JsonResponse(['token' => $token], 200);
    }
}
