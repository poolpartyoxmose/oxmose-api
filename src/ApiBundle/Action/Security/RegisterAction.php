<?php

namespace ApiBundle\Action\Security;


use AppBundle\Service\UserService;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\User;

class RegisterAction
{


    /**
     * @var UserService
     */
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * On récupère les infos de register. On les traite et une fois que c'est prêt on retourne l'utilisateur pour qu'il
     * soit validé, persisté puis on envoi un token de connexion
     * @Route(
     *     name="api_user_register",
     *     path="/register",
     *     defaults = {"_api_resource_class"=User::class, "_api_collection_operation_name"="user_register"},
     *      methods={"POST"}
     * )
     */
    public function __invoke(User $data)
    {
        $user = $this->userService->saveUser($data);
        return $user;
    }
}
