<?php

namespace ApiBundle\Action\Security;

use AppBundle\Service\SecurityService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use ApiBundle\Exception\ApiException;

class ResetPasswordAction
{

    /**
     * @Route(name="api_reset_password", path="/password/reset")
     */
    public function __invoke(Request $request, SecurityService $securityService)
    {
        $data = json_decode($request->getContent(), true);

        $token = $data['token'];
        $plainPassword = $data['password'];
        $user = $securityService->getUserByToken($token);
        if(!$token || !$plainPassword)
        {
            throw new ApiException('Merci d\'indiquer un token ainsi que le mot de passe à modifier');
        }

        if($user !==null) {
            $user->setPlainPassword($plainPassword);
            $securityService->savePlainPassword($user);
        }

        return new JsonResponse(['status' => 'success'], 200);

    }
}
