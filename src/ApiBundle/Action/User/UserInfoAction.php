<?php

namespace ApiBundle\Action\User;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use AppBundle\Entity\User;

class UserInfoAction
{


    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @Route(
     *     path="/me",
     *     name="api_user_info",
     *     defaults = {"_api_resource_class"=User::class, "_api_collection_operation_name"="user_info"}
     * )
     * @Method("GET")
     */
    public function __invoke()
    {
        $user = $this->tokenStorage->getToken()->getUser();
        return $user;
    }
}
