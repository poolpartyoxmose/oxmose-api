<?php

namespace ApiBundle\Exception;


class ApiException extends \Exception
{

    public function getJsonMessage()
    {
        return [
            'code' => 400,
            'message' => $this->getMessage()
        ];
    }
}
