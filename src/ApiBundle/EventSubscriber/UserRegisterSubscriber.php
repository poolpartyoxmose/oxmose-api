<?php
namespace ApiBundle\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use AppBundle\Entity\User;
use AppBundle\Service\SecurityService;
use AppBundle\Service\UserService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Controle que l'on est bien sur la route /api/register et enregistre l'utilisateur et renvoi un token de connexion
 *
 * Class UserRegisterSubscriber
 * @package ApiBundle\EventSubscriber
 */
class UserRegisterSubscriber implements EventSubscriberInterface
{

    /**
     * @var SecurityService
     */
    private $securityService;
    /**
     * @var UserService
     */
    private $userService;

    public function __construct(SecurityService $securityService, UserService $userService)
    {
        $this->securityService = $securityService;
        $this->userService = $userService;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['postWriteUser', EventPriorities::POST_WRITE],
        ];
    }

    /**
     * On controle que l'on est bien sur la route /api/register. On sait que l'utilisateur a été enregistré.
     * On renvoi un token au lieu de renvoyer l'objet utilisateur
     * @param GetResponseForControllerResultEvent $event
     */
    public function postWriteUser(GetResponseForControllerResultEvent $event) {
        $request = $event->getRequest();

        if(Request::METHOD_POST !== $request->getMethod() && $request->getUri() !== '/api/register') {
            return;
        }
        $user = $event->getControllerResult();
        if(!$user instanceof User) {
            return;
        }

        $event->setResponse(new JsonResponse(['token' => $this->userService->generateToken($user)], 201));
        $event->stopPropagation();
    }
}
