<?php

namespace ApiBundle\Authenticator;

use AppBundle\Entity\User;

interface AuthServiceInterface
{

    public function login($token);

    public function generateToken(User $user);
}
