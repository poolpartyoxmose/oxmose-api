<?php

namespace ApiBundle\Authenticator;


use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;

class GoogleAuth implements AuthServiceInterface
{

    /**
     * @var string
     */
    private $googleClientID;
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var JWTManager
     */
    private $JWTManager;

    public function __construct($googleClientID, EntityManager $entityManager, JWTManager $JWTManager)
    {
        $this->googleClientID = $googleClientID;
        $this->entityManager = $entityManager;
        $this->JWTManager = $JWTManager;
    }

    public function login($token)
    {
        $client = new \Google_Client(['client_id' => $this->googleClientID]);  // Specify the CLIENT_ID of the app that accesses the backend
        $payload = $client->verifyIdToken($token);
        if ($payload) {
            $userid = $payload['sub'];
            $user = $this->entityManager->getRepository('AppBundle:User')->findOneByGoogleId($userid);

            if($user === null) {
                $user = new User();
                $user->setEnabled(true);
                $user->setRoles(['ROLE_USER']);
                $user->setEmail($payload['email']);
                $user->setGoogleId($userid);
                $user->setGoogleToken($token);
                $user->setUsername($payload['given_name']);
                $user->setFirstName($payload['given_name']);
                $user->setLastName($payload['family_name']);
                $this->entityManager->persist($user);
                $this->entityManager->flush($user);
            }

            return $this->generateToken($user);
        } else {
            return null;

        }
    }

    public function generateToken(User $user)
    {
        return $this->JWTManager->create($user);
    }

}
