<?php

namespace ApiBundle\Authenticator;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;

class FacebookAuth implements AuthServiceInterface
{
    private $userInfoUrl = 'https://graph.facebook.com/me';
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var JWTManager
     */
    private $JWTManager;


    public function __construct(EntityManager $entityManager, JWTManager $JWTManager)
    {
        $this->entityManager = $entityManager;
        $this->JWTManager = $JWTManager;
    }

    public function login($token)
    {
        $client = new Client();
        $response = $client->get($this->userInfoUrl, [RequestOptions::QUERY => [
            'access_token' => $token, 'fields' => 'email,first_name,last_name'
        ]]);

        if ($response->getStatusCode() !== 400) {
            $userArray = json_decode($response->getBody()->getContents(), true);
            $user = $this->entityManager->getRepository('AppBundle:User')->findOneByFacebookId($userArray['id']);

            if ($user === null) {
                $user = new User();
                $user->setEmail($userArray['email']);
                $user->setFacebookId($userArray['id']);
                $user->setFacebookToken($token);
                $user->setRoles(['ROLE_USER']);
                $user->setEnabled(true);
                $user->setFirstName($userArray['first_name']);
                $user->setLastName($userArray['last_name']);
                $user->setUsername($userArray['first_name']);
                $this->entityManager->persist($user);
                $this->entityManager->flush($user);
            }

            return $this->generateToken($user);
        }
        return null;
    }

    public function generateToken(User $user)
    {
        return $this->JWTManager->create($user);
    }

}
